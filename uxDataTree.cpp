﻿// uxDataTree.cpp: 实现文件
//

#include "stdafx.h"
#include "resource.h"
#include "uxDataTree.h"


// uxDataTree

IMPLEMENT_DYNAMIC(uxDataTree, CTreeCtrl)

uxDataTree::uxDataTree()
{
	m_hItemClicked = NULL;
}

uxDataTree::~uxDataTree()
{
}


BEGIN_MESSAGE_MAP(uxDataTree, CTreeCtrl)
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(NM_RCLICK, &uxDataTree::OnNMRClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &uxDataTree::OnNMDblclk)
	ON_NOTIFY_REFLECT(NM_CLICK, &uxDataTree::OnNMClick)
	ON_COMMAND(IDR_RMENU_FEATURECLASS_PROPERTY, &uxDataTree::OnFeatureClassProperty)
	ON_UPDATE_COMMAND_UI(IDR_RMENU_FEATURECLASS_PROPERTY, &uxDataTree::OnUpdateFeatureClassProperty)
END_MESSAGE_MAP()



// uxDataTree 消息处理程序




int uxDataTree::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CTreeCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码
	BOOL ret = m_imageList.Create(IDB_UX_MAPTREE_BITMAP, 16, 112, RGB(255, 255, 255));

	if (ret)
	{
		CImageList* pList = SetImageList(&m_imageList, TVSIL_NORMAL);
	}

	HTREEITEM  hRoot = InsertItem(_T("数据"), 0, 0);
	SetItemState(hRoot, INDEXTOSTATEIMAGEMASK(0), TVIS_STATEIMAGEMASK);
	SetItemData(hRoot, 0);
	SetCheck(hRoot, 1);
	Expand(hRoot, TVE_EXPAND);

	return 0;
}

void uxDataTree::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CTreeCtrl::OnLButtonDown(nFlags, point);
}


void uxDataTree::OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;

	CPoint pt;
	CPoint ptSc;//右击菜单的右上角的位置是基于屏幕坐标系  
	UINT  flag;
	GetCursorPos(&pt); //获取当前点击坐标的全局坐标 
	ptSc = pt;
	this->ScreenToClient(&pt);
	//MapWindowPoints(this, &pt, 1);//MapWindowPoint  为父类（CDialog）的成员函数,  将坐标系映射为CTreeCtrl的坐标系

	HTREEITEM hItem = HitTest(pt, &flag);
	m_hItemClicked = hItem;
	if (hItem)
	{
		uxDataNodeItemData* pItemData = (uxDataNodeItemData *)GetItemData(hItem);
		if (pItemData != NULL)
		{
			uxDataTreeNodeType type = pItemData->GetType();
			switch (type)
			{
			case DTNFeatureClass:
			{
				CPoint pt;
				GetCursorPos(&pt); //获取当前点击坐标的全局坐标 

				CMenu m, *mn;
				m.LoadMenu(IDR_RMENU_DATA_FEATURECLASS);//加载菜单资源
				mn = m.GetSubMenu(0);//获取菜单子项
				mn->TrackPopupMenu(TPM_LEFTALIGN, pt.x, pt.y, this);    //显示菜单
			}
			break;
			}

		}
	}
}


void uxDataTree::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}


void uxDataTree::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}


BOOL uxDataTree::AddDataStore(u::DataStore* pDataStore)
{
	const ux_char* szName = pDataStore->GetName();
	if (FindDataStore(szName))
	{
		return FALSE;
	}

	HTREEITEM hRoot = GetRootItem();
	HTREEITEM hStore= InsertItem(szName, 3, 3, hRoot);

	uxDataNodeItemData* pItemData = new uxDataNodeItemData(DTNFeatureStore, static_cast<u::FeatureDataStore*>(pDataStore));
	SetItemData(hStore, (DWORD_PTR)pItemData);
	

	PopulateDataset(hStore, pDataStore);

	Expand(hRoot, TVE_EXPAND);

	return TRUE;
}

BOOL uxDataTree::FindDataStore(const ux_char* szName)
{
	return FALSE;
}

void uxDataTree::PopulateDataset(HTREEITEM hStore, u::DataStore* pDataStore)
{
	u::uDataStoreType type = pDataStore->GetType();
	switch (type)
	{
	case u::DataStoreTypeEsriGDB:
	case u::DataStoreTypeEsriPDB:
	{
		u::FeatureClass* pFeatureClass = NULL;
		u::FeatureDataStore* pFeatureStore = static_cast<u::FeatureDataStore*>(pDataStore);
		ux_uint count = pFeatureStore->GetFeatureClassCount();
		for (ux_uint i = 0; i < count; i++)
		{
			pFeatureClass = pFeatureStore->GetFeatureClass(i);
			HTREEITEM hItem = InsertItem(pFeatureClass->GetName(), 1, 1, hStore);

			uxDataNodeItemData* pItemData = new uxDataNodeItemData(DTNFeatureClass, pFeatureClass);
			SetItemData(hItem, (DWORD_PTR)pItemData);
		}
	}
		break;
	}
	Expand(hStore, TVE_EXPAND);
}

uxDataTreeNodeType uxDataTree::GetDataTreeNodeType(u::uDataStoreType type)
{
	uxDataTreeNodeType dType = DRNUnknown;
	switch (type)
	{
	case u::DataStoreTypeEsriSHP:
	case u::DataStoreTypeEsriGDB:
	case u::DataStoreTypeEsriPDB:
		dType = DTNFeatureStore;
		break;
	default:
		break;
	}
	return dType;
}




void uxDataTree::OnFeatureClassProperty()
{
	// TODO: 在此添加命令处理程序代码
	if (m_hItemClicked == NULL)
	{
		return;
	}
	HTREEITEM hItem = m_hItemClicked;
	uxDataNodeItemData* pItemData = (uxDataNodeItemData *)GetItemData(hItem);
	if (pItemData)
	{
		u::FeatureClass* pFeatureClass = pItemData->GetFeatureClass();
		if (pFeatureClass)
		{
			AfxMessageBox(pFeatureClass->GetName());
		}
	}
}


void uxDataTree::OnUpdateFeatureClassProperty(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}
