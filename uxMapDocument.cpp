﻿// uDocument.cpp: 实现文件
//

#include "stdafx.h"
#include "uxMapDocument.h"


// uDocument

IMPLEMENT_DYNCREATE(uxMapDocument, CDocument)

uxMapDocument::uxMapDocument()
{
	m_pMap = new u::Map();
}

BOOL uxMapDocument::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

uxMapDocument::~uxMapDocument()
{
	if (m_pMap != NULL)
	{
		m_pMap->Close();
		m_pMap->Release();
		m_pMap = NULL;
	}
}


BEGIN_MESSAGE_MAP(uxMapDocument, CDocument)
END_MESSAGE_MAP()


// uDocument 诊断

#ifdef _DEBUG
void uxMapDocument::AssertValid() const
{
	CDocument::AssertValid();
}

#ifndef _WIN32_WCE
void uxMapDocument::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif
#endif //_DEBUG

#ifndef _WIN32_WCE
// uDocument 序列化

void uxMapDocument::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO:  在此添加存储代码
	}
	else
	{
		// TODO:  在此添加加载代码
	}
}
#endif


// uDocument 命令

u::Map* uxMapDocument::GetMap()
{
	return m_pMap;
}