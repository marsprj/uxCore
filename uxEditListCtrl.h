#pragma once

#include "uxEdit.h"
// uxEditListCtrl

class AFX_EXT_CLASS uxEditListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(uxEditListCtrl)

public:
	uxEditListCtrl();
	virtual ~uxEditListCtrl();

protected:
	uxEdit	m_edit;
	int		m_row;
	int		m_col;

public:
	void	GetModifiedText(CString& strText);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnHdnItemdblclick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnEditEnd(WPARAM wParam, LPARAM lParam);
};


