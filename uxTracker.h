#pragma once

#include "uCore/uViewer.h"
#include "uCore/uEnvelope.h"

class uxTracker
{
public:
	uxTracker(HWND hWnd, HDC hdc, u::Viewer* pViewer);
	virtual ~uxTracker();

public:
	u::Envelope*		TrackRectangle();
	OGRPolygon*			TrackPolygon();

	u::Envelope*		TrackRectangle(POINT spoint);
	OGRLineString*		TrackSegment(POINT spoint);
	OGRPolygon*			TrackPolygon(POINT spoint);

private:
	void				DrawLine(HDC hdc, POINT& start, POINT& end);
	void				DrawRectangle(HDC hdc, POINT& ll, POINT& ur);

private:
	HDC			m_hdc;
	HWND		m_hWnd;
	u::Viewer*	m_pViewer;
};

