#pragma once


// uxEdit
#define	WM_UX_EDIT_END	WM_USER + 1

class AFX_EXT_CLASS uxEdit : public CEdit
{
	DECLARE_DYNAMIC(uxEdit)

public:
	uxEdit();
	virtual ~uxEdit();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};


