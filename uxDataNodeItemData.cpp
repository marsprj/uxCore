#include "stdafx.h"
#include "uxDataNodeItemData.h"

uxDataNodeItemData::uxDataNodeItemData(uxDataTreeNodeType type, u::FeatureDataStore* pFeatureStore)
{
	m_type = type;
	m_pFeatureStore = pFeatureStore;
	m_pFeatureClass = NULL;
}

uxDataNodeItemData::uxDataNodeItemData(uxDataTreeNodeType type, u::FeatureClass* pFeatureClass)
{
	m_type = type;
	m_pFeatureStore = NULL;
	m_pFeatureClass = pFeatureClass;
}

uxDataNodeItemData::~uxDataNodeItemData()
{
}

uxDataTreeNodeType uxDataNodeItemData::GetType()
{
	return m_type;
}

u::FeatureDataStore* uxDataNodeItemData::GetFeatureStore()
{
	return m_pFeatureStore;
}

u::FeatureClass* uxDataNodeItemData::GetFeatureClass()
{
	return m_pFeatureClass;
}