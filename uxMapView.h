﻿#pragma once

// uView 视图

#include "uCore/uCore.h"
#include "uCore/uMapCanvas.h"

typedef enum MapOperation
{
	operNone = 0,
	operZoomIn,
	operZoomOut,
	operWheelIn,
	operWheelOut,
	operPan,
	operFullScreen,

	operQueryByPoint,
	operQueryByRect,
	operQueryByPolygon,

	operAnalyisProfile,

	operMeshNodeAdd,			//增加节点
	operMeshNodeRemove,			//删除节点

	operMeshNodeWaterHead,		//节点水位
	operMeshNodeDispacement,	//节点形变

	operTinNodeParameter
}MapOperation;

class AFX_EXT_CLASS uxMapView : public CView
{
	DECLARE_DYNCREATE(uxMapView)

protected:
	uxMapView();           // 动态创建所使用的受保护的构造函数
	virtual ~uxMapView();

public:
	virtual void OnDraw(CDC* pDC);      // 重写以绘制该视图
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	u::Map*		m_pMap;
	u::Viewer	m_viewer;
	bool		m_refresh;

	ux_int		m_pan_offset_x;
	ux_int		m_pan_offset_y;

	double		m_zoomin_f;
	double		m_zoomout_f;

	u::Color	m_bgColor;
	Gdiplus::Bitmap* m_pBitmap;

	MapOperation	m_mapOperation;
	POINT		m_dn_point, m_up_point;
	bool		m_paning;

public:
	void		AttachMap(u::Map* pMap);
	void		Refresh(bool force = false);

	void		SetViewer(u::Envelope& viewer);
	u::Envelope	GetViewer();

	void		OnZoomIn();
	void		OnZoomOut();
	void		OnPan();
	void		OnFullScreen();
	void		OnReset();

	void		SetMapOperation(MapOperation oper);
	MapOperation GetMapOperation();
	
protected:
	void		DrawBackground();
	void		DrawMap(CRect& viewRect);
	void		DrawCachedMap(u::MapCanvas &canvas, CRect& viewRect, /*临时使用*/u::GraphicsGdiplus* pGraphics);
	
	//void		DrawSelection(cnu::GCanvas& canvas);

	void		ZoomIn(CPoint point);
	void		ZoomOut(CPoint point);
	void		Pan(CPoint point);


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
};


