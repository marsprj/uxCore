#pragma once

typedef enum uxDataTreeNodeType
{
	DRNUnknown = 0,
	DTNFeatureStore,
	DTNFeatureClass,
	DTNRasterStore,
	DTNRasterSet,
	DTNRaster,
	DRNRasterBand
}uxDataTreeNodeType;

class AFX_EXT_CLASS uxDataNodeItemData
{
public:
	uxDataNodeItemData(uxDataTreeNodeType type, u::FeatureDataStore* pFeatureStore);
	uxDataNodeItemData(uxDataTreeNodeType type, u::FeatureClass*     pFeatureClass);
	virtual ~uxDataNodeItemData();

public:
	uxDataTreeNodeType		GetType();
	u::FeatureDataStore*	GetFeatureStore();
	u::FeatureClass*		GetFeatureClass();

public:
	uxDataTreeNodeType		m_type;
	u::FeatureDataStore*	m_pFeatureStore;
	u::FeatureClass*		m_pFeatureClass;
};

