// uxEditListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "uxEditListCtrl.h"


// uxEditListCtrl

IMPLEMENT_DYNAMIC(uxEditListCtrl, CListCtrl)

uxEditListCtrl::uxEditListCtrl()
{
	m_row = -1;
	m_col = -1;
}

uxEditListCtrl::~uxEditListCtrl()
{
}


BEGIN_MESSAGE_MAP(uxEditListCtrl, CListCtrl)
	ON_WM_CREATE()
	ON_NOTIFY_REFLECT(NM_CLICK, &uxEditListCtrl::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &uxEditListCtrl::OnNMDblclk)
	ON_NOTIFY(HDN_ITEMDBLCLICKA, 0, &uxEditListCtrl::OnHdnItemdblclick)
	ON_NOTIFY(HDN_ITEMDBLCLICKW, 0, &uxEditListCtrl::OnHdnItemdblclick)
	ON_MESSAGE(WM_UX_EDIT_END, OnEditEnd)
END_MESSAGE_MAP()



// uxEditListCtrl message handlers




int uxEditListCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	ModifyStyle(0, LVS_REPORT | LVS_SINGLESEL);
	SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	//SetFont(GetFont());

	m_edit.Create(ES_AUTOHSCROLL | WS_CHILD | ES_LEFT | ES_WANTRETURN | WS_BORDER | WS_EX_CLIENTEDGE, CRect(0, 0, 0, 0), this, IDC_LISTCTRL_EDIT);
	m_edit.ShowWindow(SW_HIDE);

	return 0;
}


void uxEditListCtrl::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	int nItem = pNMListView->iItem;

	CString strFID = GetItemText(nItem, 0);
	long fid = atoi(strFID);

}


void uxEditListCtrl::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here

	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	m_row = pNMListView->iItem;
	m_col = pNMListView->iSubItem;

	//if (m_col == 0)
	//{
	//	//第一列FID字段，不允许编辑
	//}
	//else
	{
		CRect rect;
		GetSubItemRect(m_row, m_col, LVIR_LABEL, rect);
		CString strText;
		strText = GetItemText(m_row, m_col);
		rect.top += 2;
		m_edit.MoveWindow(&rect);
		m_edit.SetWindowText(strText);
		m_edit.ShowWindow(SW_SHOW);
		m_edit.SetFocus();
		m_edit.SetSel(strText.GetLength());
	}

	*pResult = 0;
}


void uxEditListCtrl::OnHdnItemdblclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: Add your control notification handler code here
	AfxMessageBox("OK");
	*pResult = 0;
}

LRESULT uxEditListCtrl::OnEditEnd(WPARAM wParam, LPARAM lParam)
{
	if (m_edit.GetSafeHwnd())
	{
		m_edit.ShowWindow(SW_HIDE);
		CString strText;
		m_edit.GetWindowText(strText);

		//更新ListCtrl单元格的值
		SetItemText(m_row, m_col, strText);

	}
	return 0;
}

void uxEditListCtrl::GetModifiedText(CString& strText)
{
	m_edit.GetWindowText(strText);
}