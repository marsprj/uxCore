﻿// uDocument.cpp: 实现文件
//

#include "stdafx.h"
#include "uMapDocument.h"


// uDocument

IMPLEMENT_DYNCREATE(uMapDocument, CDocument)

uMapDocument::uMapDocument()
{
}

BOOL uMapDocument::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

uMapDocument::~uMapDocument()
{
}


BEGIN_MESSAGE_MAP(uMapDocument, CDocument)
END_MESSAGE_MAP()


// uDocument 诊断

#ifdef _DEBUG
void uMapDocument::AssertValid() const
{
	CDocument::AssertValid();
}

#ifndef _WIN32_WCE
void uMapDocument::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif
#endif //_DEBUG

#ifndef _WIN32_WCE
// uDocument 序列化

void uMapDocument::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO:  在此添加存储代码
	}
	else
	{
		// TODO:  在此添加加载代码
	}
}
#endif


// uDocument 命令
