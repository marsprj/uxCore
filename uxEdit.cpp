// uxEdit.cpp : implementation file
//

#include "stdafx.h"
#include "uxEdit.h"


// uxEdit

IMPLEMENT_DYNAMIC(uxEdit, CEdit)

uxEdit::uxEdit()
{

}

uxEdit::~uxEdit()
{
}


BEGIN_MESSAGE_MAP(uxEdit, CEdit)
	ON_WM_KILLFOCUS()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()



// uxEdit message handlers




void uxEdit::OnKillFocus(CWnd* pNewWnd)
{
	CEdit::OnKillFocus(pNewWnd);

	// TODO: Add your message handler code here
	CWnd* pParent = this->GetParent();
	::PostMessage(pParent->GetSafeHwnd(), WM_UX_EDIT_END, 0, 0);
}


void uxEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}
