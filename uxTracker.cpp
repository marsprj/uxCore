#include "stdafx.h"
#include "uxTracker.h"


uxTracker::uxTracker(HWND hWnd, HDC hdc, u::Viewer* pViewer)
{
	m_hWnd = hWnd;
	m_hdc = hdc;
	m_pViewer = pViewer;
}


uxTracker::~uxTracker()
{
}

u::Envelope* uxTracker::TrackRectangle()
{
	COLORREF color = RGB(0, 0, 255);
	int width = 2;

	MSG msg;
	POINT down_point, move_point, point;
	u::Envelope *pEnvelope = NULL;

	while (GetMessage(&msg, m_hWnd, WM_MOUSEMOVE, WM_RBUTTONDOWN))
	{
		point.x = (short)(LOWORD(msg.lParam));
		point.y = (short)(HIWORD(msg.lParam));

		switch (msg.message)
		{
		case WM_LBUTTONDOWN:
		{
			if (::GetCapture() != m_hWnd)
			{
				move_point = down_point = point;
				::SetROP2(m_hdc, R2_NOTXORPEN);

				CPen pen;
				pen.CreatePen(PS_SOLID, width, color);
				HGDIOBJ hold = ::SelectObject(m_hdc, pen.GetSafeHandle());

				DrawRectangle(m_hdc, down_point, move_point);

				::SelectObject(m_hdc, hold);

				::SetCapture(m_hWnd);
			}
		}
		break;
		case WM_MOUSEMOVE:
		{
			if (::GetCapture() == m_hWnd)
			{
				CPen pen;
				pen.CreatePen(PS_SOLID, width, color);
				HGDIOBJ hold = ::SelectObject(m_hdc, pen.GetSafeHandle());

				DrawRectangle(m_hdc, down_point, move_point);
				move_point = point;
				DrawRectangle(m_hdc, down_point, move_point);

				::SelectObject(m_hdc, hold);
			}
		}
		break;
		case WM_LBUTTONUP:
		{
			if (::GetCapture() == m_hWnd)
			{
				CPen pen;
				pen.CreatePen(PS_SOLID, width, color);
				HGDIOBJ hold = ::SelectObject(m_hdc, pen.GetSafeHandle());

				DrawRectangle(m_hdc, down_point, move_point);

				::SelectObject(m_hdc, hold);

				::ReleaseCapture();

				move_point = point;

				pEnvelope = new u::Envelope();
				pEnvelope->Set(down_point.x, down_point.y, move_point.x, move_point.y);
				return pEnvelope;
			}

		}
		case WM_LBUTTONDBLCLK:
		{

		}
		break;
		}
	}

	return NULL;
}

OGRPolygon* uxTracker::TrackPolygon()
{
	MSG msg;
	POINT down_point, move_point, point;
	u::Envelope *pEnvelope = NULL;

	while (GetMessage(&msg, m_hWnd, WM_MOUSEMOVE, WM_RBUTTONDOWN))
	{
		point.x = (short)(LOWORD(msg.lParam));
		point.y = (short)(HIWORD(msg.lParam));

		switch (msg.message)
		{
		case WM_LBUTTONDOWN:
		{
			if (::GetCapture() != m_hWnd)
			{
				move_point = down_point = point;
				::SetROP2(m_hdc, R2_NOTXORPEN);

				DrawRectangle(m_hdc, down_point, move_point);

				::SetCapture(m_hWnd);
			}
		}
		break;
		case WM_MOUSEMOVE:
		{
			if (::GetCapture() == m_hWnd)
			{
				DrawRectangle(m_hdc, down_point, move_point);
				move_point = point;
				DrawRectangle(m_hdc, down_point, move_point);
			}
		}
		break;
		case WM_LBUTTONUP:
		{

		}
		case WM_LBUTTONDBLCLK:
		{
			if (::GetCapture() == m_hWnd)
			{
				DrawRectangle(m_hdc, down_point, move_point);
				::ReleaseCapture();

				move_point = point;

				pEnvelope = new u::Envelope();
				pEnvelope->Set(down_point.x, down_point.y, move_point.x, move_point.y);
				//return pEnvelope;
			}
		}
		break;
		}
	}

	return NULL;
}

u::Envelope* uxTracker::TrackRectangle(POINT spoint)
{
	COLORREF color = RGB(0, 0, 255);
	int width = 2;

	MSG msg;
	POINT down_point, move_point, point;
	u::Envelope *pEnvelope = NULL;

	HWND hCapatureWnd = ::GetCapture();

	if (::GetCapture() != m_hWnd)
	{
		move_point = down_point = spoint;
		::SetROP2(m_hdc, R2_NOTXORPEN);

		CPen pen;
		pen.CreatePen(PS_SOLID, width, color);
		HGDIOBJ hold = ::SelectObject(m_hdc, pen.GetSafeHandle());

		DrawRectangle(m_hdc, down_point, move_point);

		::SelectObject(m_hdc, hold);

		::SetCapture(m_hWnd);
	}

	while (GetMessage(&msg, m_hWnd, WM_MOUSEMOVE, WM_RBUTTONDOWN))
	{
		point.x = (short)(LOWORD(msg.lParam));
		point.y = (short)(HIWORD(msg.lParam));

		switch (msg.message)
		{
			// 		case WM_LBUTTONDOWN:
			// 			{
			// 				if(::GetCapture()!=m_hWnd)
			// 				{
			// 					move_point = down_point = point;
			// 					::SetROP2(m_hdc,R2_NOTXORPEN);
			// 					
			// 					DrawRectangle(m_hdc, down_point, move_point);
			// 					
			// 					::SetCapture(m_hWnd);
			// 				}
			// 			}
			// 			break;
		case WM_MOUSEMOVE:
		{
			if (::GetCapture() == m_hWnd)
			{
				CPen pen;
				pen.CreatePen(PS_SOLID, width, color);
				HGDIOBJ hold = ::SelectObject(m_hdc, pen.GetSafeHandle());

				DrawRectangle(m_hdc, down_point, move_point);
				move_point = point;
				DrawRectangle(m_hdc, down_point, move_point);

				::SelectObject(m_hdc, hold);
			}
		}
		break;
		case WM_LBUTTONUP:
		{
			if (::GetCapture() == m_hWnd)
			{
				CPen pen;
				pen.CreatePen(PS_SOLID, width, color);
				HGDIOBJ hold = ::SelectObject(m_hdc, pen.GetSafeHandle());

				DrawRectangle(m_hdc, down_point, move_point);

				::SelectObject(m_hdc, hold);
				::ReleaseCapture();

				move_point = point;

				pEnvelope = new u::Envelope();
				pEnvelope->Set(down_point.x, down_point.y, move_point.x, move_point.y);
				return pEnvelope;
			}

		}
		case WM_LBUTTONDBLCLK:
		{

		}
		break;
		}
	}

	return NULL;
}

OGRLineString* uxTracker::TrackSegment(POINT spoint)
{
	MSG msg;
	POINT down_point, move_point, point;
	u::Envelope *pEnvelope = NULL;

	HWND hCapatureWnd = ::GetCapture();

	if (::GetCapture() != m_hWnd)
	{
		move_point = down_point = spoint;
		::SetROP2(m_hdc, R2_NOTXORPEN);

		DrawLine(m_hdc, down_point, move_point);

		::SetCapture(m_hWnd);
	}

	while (GetMessage(&msg, m_hWnd, WM_MOUSEMOVE, WM_RBUTTONDOWN))
	{
		point.x = (short)(LOWORD(msg.lParam));
		point.y = (short)(HIWORD(msg.lParam));

		switch (msg.message)
		{
			// 		case WM_LBUTTONDOWN:
			// 			{
			// 				if(::GetCapture()!=m_hWnd)
			// 				{
			// 					move_point = down_point = point;
			// 					::SetROP2(m_hdc,R2_NOTXORPEN);
			// 					
			// 					DrawRectangle(m_hdc, down_point, move_point);
			// 					
			// 					::SetCapture(m_hWnd);
			// 				}
			// 			}
			// 			break;
		case WM_MOUSEMOVE:
		{
			if (::GetCapture() == m_hWnd)
			{
				DrawLine(m_hdc, down_point, move_point);
				move_point = point;
				DrawLine(m_hdc, down_point, move_point);
			}
		}
		break;
		case WM_LBUTTONUP:
		{
			if (::GetCapture() == m_hWnd)
			{
				DrawLine(m_hdc, down_point, move_point);
				::ReleaseCapture();

				move_point = point;

				OGRLineString* pLineString = new OGRLineString();
				double mx = 0.0, my = 0.0;
				m_pViewer->ToMapPoint(spoint.x, spoint.y, mx, my);
				pLineString->addPoint(new OGRPoint(mx, my));
				m_pViewer->ToMapPoint(point.x, point.y, mx, my);
				pLineString->addPoint(new OGRPoint(mx, my));

				return pLineString;
			}

		}
		case WM_LBUTTONDBLCLK:
		{

		}
		break;
		}
	}

	return NULL;
}

void uxTracker::DrawLine(HDC hdc, POINT& start, POINT& end)
{
	::MoveToEx(hdc, start.x, start.y, NULL);
	::LineTo(hdc, end.x, end.y);
}

void uxTracker::DrawRectangle(HDC hdc, POINT& ll, POINT& ur)
{
	::MoveToEx(hdc, ll.x, ll.y, NULL);

	::LineTo(hdc, ur.x, ll.y);
	::LineTo(hdc, ur.x, ur.y);
	::LineTo(hdc, ll.x, ur.y);
	::LineTo(hdc, ll.x, ll.y);
}