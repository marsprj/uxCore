#pragma once

// uxDataTree

#include "uxDataNodeItemData.h"

class AFX_EXT_CLASS uxDataTree : public CTreeCtrl
{
	DECLARE_DYNAMIC(uxDataTree)

public:
	uxDataTree();
	virtual ~uxDataTree();

protected:
	CImageList	m_imageList;
	HTREEITEM	m_hItemClicked;

public:
	BOOL		AddDataStore(u::DataStore* pDataStore);

protected:
	BOOL		FindDataStore(const ux_char* szName);
	void		PopulateDataset(HTREEITEM hStore, u::DataStore* pDataStore);
	uxDataTreeNodeType GetDataTreeNodeType(u::uDataStoreType type);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnFeatureClassProperty();
	afx_msg void OnUpdateFeatureClassProperty(CCmdUI *pCmdUI);
};


