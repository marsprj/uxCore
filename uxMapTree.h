#pragma once


// uxMapTree

class uxLayerNodeItemData
{
public:
	uxLayerNodeItemData(
		u::Layer*	pLayer,
		ux_int		type,
		bool		closable)
	{
		m_pLayer = pLayer;
		m_type = type;
		m_closable = closable;
	}
	virtual ~uxLayerNodeItemData(){}
public:
	u::Layer*	GetLayer() { return m_pLayer; }
	ux_uint		GetType() { return m_type; }
	bool		IsClosable() { return m_closable; }
public:
	u::Layer*	m_pLayer;		
	ux_int		m_type;			//图层的类型，有外部设置，与m_pLayer->GetType()不完全一致
	bool		m_closable;		//图层是否可以关闭（对于GroupLayer的子图层，不允许单独关闭)

};

class AFX_EXT_CLASS uxMapTree : public CTreeCtrl
{
	DECLARE_DYNAMIC(uxMapTree)

protected:
	u::Map*		m_pMap;
	CImageList m_imageList;
protected:
	UINT		m_TimerTicks;		//处理滚动的定时器所经过的时间
	UINT		m_nScrollTimerID;	//处理滚动的定时器
	CPoint		m_HoverPoint;		//鼠标位置
	UINT		m_nHoverTimerID;	//鼠标敏感定时器
	DWORD		m_dwDragStart;		//按下鼠标左键那一刻的时间
	BOOL		m_bDragging;		//标识是否正在拖动过程中
	CImageList* m_pDragImage;		//拖动时显示的图象列表
	HTREEITEM	m_hItemDragS;		//被拖动的标签
	HTREEITEM	m_hItemDragD;		//接受拖动的标签

public:
	uxMapTree();
	virtual ~uxMapTree();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTvnBeginrdrag(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnTvnBegindrag(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult);

public:
	void		Attach(u::Map* pMap);
	int			AddLayer(u::Layer* pLayer, ux_uint type);	

protected:
	int			GetLayerIcon(u::Layer* pLayer);

	HTREEITEM	CopyBranch(HTREEITEM htiBranch, HTREEITEM htiNewParent, HTREEITEM htiAfter);
	HTREEITEM	CopyItem(HTREEITEM hItem, HTREEITEM htiNewParent, HTREEITEM htiAfter);

	BOOL		SetVisible(HTREEITEM hItem);


public:
	

};

