﻿#pragma once

// uDocument 文档

class AFX_EXT_CLASS uxMapDocument : public CDocument
{
	DECLARE_DYNCREATE(uxMapDocument)

public:
	uxMapDocument();
	virtual ~uxMapDocument();
#ifndef _WIN32_WCE
	virtual void Serialize(CArchive& ar);   // 为文档 I/O 重写
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	u::Map*		m_pMap;

public:
	u::Map*		GetMap();

protected:
	virtual BOOL OnNewDocument();

	DECLARE_MESSAGE_MAP()
};
