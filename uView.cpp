﻿// uView.cpp: 实现文件
//

#include "stdafx.h"
#include "uMapView.h"


// uView

IMPLEMENT_DYNCREATE(uMapView, CView)

uMapView::uMapView()
{

}

uMapView::~uMapView()
{
}

BEGIN_MESSAGE_MAP(uMapView, CView)
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// uView 绘图

void uMapView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO:  在此添加绘制代码

	CRect rect;
	GetClientRect(rect);

	pDC->MoveTo(0, 0);
	pDC->LineTo(rect.Width(), rect.Height());

	u::GraphicsGdiplus g(pDC->GetSafeHdc());
	g.DrawLine(rect.Width(), 0, 0, rect.Height());
}


// uView 诊断

#ifdef _DEBUG
void uMapView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void uMapView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// uView 消息处理程序


BOOL uMapView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here and/or call default

	return CView::OnMouseWheel(nFlags, zDelta, pt);
}


void uMapView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CView::OnMouseMove(nFlags, point);
}


void uMapView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CView::OnLButtonDblClk(nFlags, point);
}


void uMapView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CView::OnLButtonDown(nFlags, point);
}


void uMapView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}
